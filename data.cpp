#include <string>
#include <iostream>
#include <fstream>

#include "xmp-scrobbler.h"

// return true if tags were extracted properly, false if they were not

std::string lowercase( const std::string& word )
{
    std::string ret_str;

  for (unsigned int index = 0; index < word.size(); index++)
    ret_str += tolower(word[index]);
 
  return ret_str;
}

// returns specified data block from XMPlay info1
// NOTE: block has empty line at the end so that extracting tags is easier

std::string XMP_GetDataBlock(std::string name, const std::string& source)
{
    const std::string& temp = lowercase(source);
	name = lowercase(name) + ":\n";

	auto start = temp.find(name, 0);

    if( start == std::string::npos )
		return "";

	start += name.length();

	auto next_start = source.find(":\n", start);
    auto block_end = source.length();

        if(next_start != std::string::npos)
		block_end = source.find_last_of("\n", next_start);

	return source.substr(start, block_end - start - 1) + '\n';
}

// extracts the value of the field fn (fn\tvalue)

bool XMP_GetTagField(const std::string& data, const char *fn, std::string& buf)
{
	const int fnlen = strlen( fn );
    const std::string& tmp = "\n" + lowercase( data );

	auto start = tmp.find( fn, 0 );
		
    if( start == std::string::npos )
		return false;

	start += fnlen;

	auto end = tmp.find("\n", start);

	buf = data.substr(start - 1, end - start);

	return true;
}

bool XMP_ExtractTags_ID3v1(const char *data, std::string& artist, std::string& title, std::string& album)
{
    const std::string& block = XMP_GetDataBlock( "ID3v1", data );

	if(!XMP_GetTagField(block, "\nartist\t", artist))
        return false;

    if(!XMP_GetTagField(block, "\ntitle\t", title))
        return false;

    // album tag is optional

	XMP_GetTagField(block, "\nalbum\t", album);
	
	return true;
}

bool XMP_ExtractTags_ID3v2(const char *data, std::string& artist, std::string& title, std::string& album)
{
    const std::string& block = XMP_GetDataBlock( "ID3v2", data );

	if(!XMP_GetTagField(block, "\nartist\t", artist))
        return false;

    if(!XMP_GetTagField(block, "\ntitle\t", title))
        return false;

    // album tag is optional

	XMP_GetTagField(block, "\nalbum\t", album);
	
	return true;
}

bool XMP_ExtractTags_WMA(const char *data, std::string& artist, std::string& title, std::string& album)
{
    const std::string tmp(data);

	if(!XMP_GetTagField(tmp, "\nauthor\t", artist))
        return false;

    if(!XMP_GetTagField(tmp, "\ntitle\t", title))
        return false;

    // album tag is optional

	XMP_GetTagField(tmp, "\nwm/albumtitle\t", album);
	
	return true;
}

bool XMP_ExtractTags_Other(const std::string& data, std::string& artist, std::string& title, std::string& album)
{
	if (!XMP_GetTagField(data, "\nartist\t", artist))
        return false;

	if (!XMP_GetTagField(data, "\ntitle\t", title))
        return false;

    // album tag is optional

	XMP_GetTagField(data, "\nalbum\t", album);
	
	return true;
}

extern bool XMP_IsRadio;               /* xmp-scrobbler.cpp */ 
extern bool RadioScrobbling;           /*                   */

/* Function of extracting datas from Internet radio string */
bool XMP_ExtractTags_NetRadio(const char *data, std::string& artist, std::string& title, std::string& album)
{
     /* Scrobbling of radio streams is disabled by user */
     if(!RadioScrobbling)
        return false;
        
     std::string tmp(data), track;
     std::string char_tmp;
     int i, j;
     DWORD find_me;
         
     /* XXX Let's think that:
      * 1) Name field appears only in NetRadio strings;
      * 2) Name field is always filled as other do.
      * Never checked both of these thougs, as i currently
      * listen only few stations which does 
      */
     if(!XMP_GetTagField(tmp, "\nname\t", album))
         return false;

     /* OGG streams workaround (and other that have valid separated fields) */
     if(XMP_ExtractTags_Other(data, artist, title, char_tmp)) {
         return XMP_IsRadio = true;
     }

     /* XXX Let's presume that Track field always contains
      * track title (or artist + title) instead of anyhing else
      */   
     if(!XMP_GetTagField(tmp, "\ntrack\t", char_tmp))
         return false;
     
     track += char_tmp;

     find_me = track.find(" - ");         
     /* XXX No artist? */
     if(find_me == std::string::npos)
         return false;

     /* Split title and artist from Track 
      * Pure C jokes :D */
     i = track.length();
	 j = find_me + 3;

     artist = char_tmp.substr(0, find_me);
	 title = char_tmp.substr(j, i);
     
     /* This is radio now! */
     return XMP_IsRadio = true;
}

bool XMP_ExtractTags_MBID(const char *data, std::string& mb)
{
	//string block = XMP_GetDataBlock( "ID3v2", data );
    std::string block(data);

	bool status = XMP_GetTagField(block, "\nunique id\t", mb);

	// OGG / FLAC - check for "MUSICBRAINZ_TRACKID" field

	if(!status)
		status = XMP_GetTagField(block, "\nmusicbrainz_trackid\t", mb);

	// M4A - check for "MusicBrainz Track Id" field

	if(!status)
		status = XMP_GetTagField(block, "\nmusicbrainz track id\t", mb);

	if(status)
	{
		auto sepPos = mb.find(" ", 0); // find owner / ID separator

        if(sepPos != std::string::npos)
			mb = mb.substr(sepPos + 1, mb.length() - sepPos );

		if(mb.length() == 36) // we possibly have a proper MBID
		{
			return true;
		} 
		else
		{
			mb.clear();
			return false;
		}
	}

	return false;
}
