#!/bin/sh
set -e

DIRN="$(dirname $0)"

mkdir -p Release
meson --buildtype=plain --strip --cross-file "$DIRN"/mingw_cross.txt "$DIRN"
ninja -v
DESTDIR=./Release ninja install -v
