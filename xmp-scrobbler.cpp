// xmp-scrobbler

#include <windows.h>
#include <commctrl.h>

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <memory>
#include <regex>

#include "xmpdsp.h"
#include "resource.h"
#include "xmp-scrobbler.h"
#include "data.h"
#include "stringify.h"


/* We need thread for our radio stream watchdog */



#include "libscrobbler/scrobbler.h"

#define XMPSCROBBLER_VERSION		"0.9.8.8"
#define MAX_LENGTH					240		// in seconds
#define DELAY_BEFORE_FETCHINFO		100		// +- 5 seconds, in ??

HINSTANCE hinst;

std::unique_ptr<Scrobbler> scrob;

std::string pathLog;
std::string pathCache;

DWORD xmpChans = 0;
DWORD xmpRate = 0;
DWORD secLength = 0;    // Second length is sample count

int xmpCounter = -1;    // submit counter - after it reaches 0 song is submitted

// CUE counter - set up as a DSP_NewTrack replacement in files with CUE sheets
// because XMPlay doesn't inform us when next track in CUE is played
// so we have to inform ourselves

int cueCounter = -1;

/* Don't scrobble radio tracks by default */
bool RadioScrobbling;
/* We need another scrobbling rules for radio tracks, as we don't know
 * real time of tracks */
bool XMP_IsRadio = false;
/* Track changed */
bool PleaseDie = false;
bool XMP_RadioNewTrack_flag = false;
unsigned int XMP_RadioTrackLength = 0;
/* Don't want to lose it */

bool WatchdogCreated = false;
unsigned int xmpDoFetchinfo = DELAY_BEFORE_FETCHINFO; //do fetchinfo inside dsp_process

void XMP_SetDoFetchInfo()
{
    xmpDoFetchinfo = DELAY_BEFORE_FETCHINFO;
}

void XMP_CancelFetchInfo()
{
    xmpDoFetchinfo = -1;
}

/* Class with struct meaning. Storing curtrack info */
class RadioStr {
private:
    std::string artist;
    std::string title;
    std::string album;
public:
    void fill_fields(std::string a, std::string t, std::string l)
    {
        artist.swap(a); title.swap(t); album.swap(l);
    }
    std::string get_artist_field() const {return artist;}
    std::string get_title_field() const {return title;}
    std::string get_album_field() const {return album;}
};
/* Ugly, but right here */
RadioStr *RadioSt = new RadioStr();


// All played music can be represented as two-level array.
// First level is FILE. Second one - TRACK. Most FILEs have only one TRACK inside,
// but some of them has CUE sheet attached and describing which TRACKs are inside.

// XMPTrack sctructure represents one TRACK from FILE.
struct XMPTrackInfo 
{
    int start = 0;       // Track start in seconds
    int length = 0;      // Length of current track in seconds
    time_t playtime = 0; // When user listened to this track
    bool tags_ok = false;
    bool submitted = false;
    bool dirty = false;

    std::string artistName;// Artist
    std::string songTitle; // Song title
};

// XMPFile represents current FILE with all TRACKs inside.
struct XMPFileInfo
{
    int current_track = -1;  // Last track played
    int length = 0;      // Total length (do we need it?)

    std::string albumName; // Album name
    std::string mbId;    // MusicBrainz ID

    std::vector<XMPTrackInfo> alltracks;// Track array
};

XMPFileInfo xmpFile;

static XMPScrobblerConfig xmpcfg;

void XMP_SetSubmitTimer();
void XMP_KillSubmitTimer();

void XMP_SetCUETimer();
void XMP_KillCUETimer();

void XMP_ClearLog();
bool XMP_ValidateLogSize();

void XMP_ScrobInit( const char *u, const char *p, const std::string& cache );
void XMP_ScrobTerm();
void XMP_SubmitProc();

void XMP_FetchInfo();

/* Radio watchdog declaration */
DWORD WINAPI XMP_RadioWatchdog(void *args);

int XMP_InTrack(int sec);

bool XMP_IsCUE();
int XMP_GetPlaybackTime();
/* Watchdog needs */
bool XMP_IsPaused();
void XMP_UpdateCurrentTrackIndex();

void XMP_CleanDirty();
void XMP_SetDirty();
bool XMP_IsDirty();
bool XMP_WasResetDirty();

void XMP_ClearXMPFile();
void XMP_Welcome();
void XMP_Setup();

XMPFUNC_MISC *xmpfmisc;
XMPFUNC_STATUS *xmpfstatus;

extern "C" {

static void *WINAPI DSP_New();
static void WINAPI DSP_Free(void *inst);
static const char *WINAPI DSP_GetDescription(void *inst);
static void WINAPI DSP_Config(void *inst, HWND win);
static DWORD WINAPI DSP_GetConfig(void *inst, void *config);
static BOOL WINAPI DSP_SetConfig(void *inst, void *config, DWORD size);
static void WINAPI DSP_NewTrack(void *inst, const char *file);
static void WINAPI DSP_SetFormat(void *inst, const XMPFORMAT *form);
static void WINAPI DSP_Reset(void *inst);
static DWORD WINAPI DSP_Process(void *inst, float *srce, DWORD count);

}




void WINAPI DSP_About(HWND win)
{
    std::string msg = "xmp-scrobbler ";

    msg += XMPSCROBBLER_VERSION;
    msg += "\n\nAdded features:\n"
           "[+] Scrobbling of radio streams";
    msg += "\n\nFixed things:\n"
           "[v] Disabling of MusicBrainz tags floodage";

    MessageBox( win, msg.c_str(), "xmp-scrobbler", MB_ICONINFORMATION );
}

XMPDSP dsp = {
    0, // doesn't support multiple instances
    "xmp-scrobbler",
    DSP_About,
    DSP_New,
    DSP_Free,
    DSP_GetDescription,
    DSP_Config,
    DSP_GetConfig,
    DSP_SetConfig,
    DSP_NewTrack,
    DSP_SetFormat,
    DSP_Reset,
    DSP_Process,
    NULL /*newtitle*/
};

// new DSP instance
void *WINAPI DSP_New()
{
    XMP_Setup();

    return (void*) 1; // no multi-instance, so just return anything not 0
}

// free DSP instance
void WINAPI DSP_Free(void */*inst*/)
{
    if( xmpcfg.on )
    {
        XMP_ScrobTerm();
    }

    XMP_Log("[DEBUG] DSP_Free()\n");

    XMP_Log("[INFO] Shutting down...\n");
    XMP_Log("----\n");
}

// get description for plugin list
const char *WINAPI DSP_GetDescription(void */*inst*/)
{
    return dsp.name;
}

#define MESS(id,m,w,l) SendDlgItemMessage(h,id,m,(WPARAM)(w),(LPARAM)(l))

BOOL CALLBACK DSPDialogProc(HWND h, UINT m, WPARAM w, LPARAM /*l*/)
{
    switch (m) {
    case WM_COMMAND:
    {
        switch (LOWORD(w))
        {
        case IDOK:
        {
            xmpcfg.on = MESS(IDC_ENABLE, BM_GETCHECK, 0, 0);

            MESS(IDC_USERNAME, WM_GETTEXT, 255, xmpcfg.username);
            MESS(IDC_PASSWORD, WM_GETTEXT, 255, xmpcfg.password);

            MESS(IDC_PROXY_SERVER, WM_GETTEXT, 255, xmpcfg.proxy_server);
            MESS(IDC_PROXY_PORT, WM_GETTEXT, 10, xmpcfg.proxy_port);

            MESS(IDC_PROXY_USER, WM_GETTEXT, 255, xmpcfg.proxy_user);
            MESS(IDC_PROXY_PASSWORD, WM_GETTEXT, 255, xmpcfg.proxy_password);

            xmpcfg.proxy_enabled = MESS(IDC_PROXY_ENABLE, BM_GETCHECK, 0, 0);
            xmpcfg.proxy_auth_enabled = MESS(IDC_PROXY_AUTH, BM_GETCHECK, 0, 0);

            xmpcfg.logfile_limit = MESS(IDC_LOGFILE_LIMIT, BM_GETCHECK, 0, 0);
            xmpcfg.logfile_truncate = MESS(IDC_LOGFILE_TRUNCATE, BM_GETCHECK, 0, 0);
            xmpcfg.logfile_debugmode = MESS(IDC_LOGFILE_DEBUGMODE, BM_GETCHECK, 0, 0);
            xmpcfg.logfile_limit_size = MESS(IDC_LOGFILE_LIMITS, CB_GETCURSEL, 0, 0);

            xmpcfg.MZ_Not_Abusive = MESS(IDC_NOT_MZ_ABUSIVE, BM_GETCHECK, 0, 0);
            /* Enable scrobbling of radio streams */
            xmpcfg.RadioScrobbling = MESS(IDC_ENABLE_RADIO_SCROBBLING, BM_GETCHECK, 0, 0);
            xmpcfg.SendAlbums = MESS(IDC_SEND_ALBUMS, BM_GETCHECK, 0, 0);
            MESS(IDC_WATCHDOG_RATE, WM_GETTEXT, 3, xmpcfg.watchdog_rate);
            xmpcfg.SendChanges = MESS(IDC_SEND_CHANGES, BM_GETCHECK, 0, 0);

            EndDialog( h, 1 );

            break;
        }

        case IDCANCEL:
        {
            EndDialog( h, 0 );
            break;
        }

        case IDC_ENABLE:
        {
            //xmpcfg.on = MESS(IDC_ENABLE, BM_GETCHECK, 0, 0);
            //MESS(IDC_PROXY_AUTH, WM_ENABLE, 0, 0);

            break;
        }

        case IDC_PROXY_ENABLE:
        {
            bool bEnabled = MESS(IDC_PROXY_ENABLE, BM_GETCHECK, 0, 0);
            bool bAuthEnabled = MESS(IDC_PROXY_AUTH, BM_GETCHECK, 0, 0);

            EnableWindow(GetDlgItem(h, IDC_PROXY_SERVER), bEnabled);
            EnableWindow(GetDlgItem(h, IDC_PROXY_PORT), bEnabled);

            EnableWindow(GetDlgItem(h, IDC_PROXY_AUTH), bEnabled);
            EnableWindow(GetDlgItem(h, IDC_PROXY_USER), bEnabled && bAuthEnabled);
            EnableWindow(GetDlgItem(h, IDC_PROXY_PASSWORD), bEnabled && bAuthEnabled);

            break;
        }

        case IDC_ENABLE_RADIO_SCROBBLING:
        {
            bool bEnabled = MESS(IDC_ENABLE_RADIO_SCROBBLING, BM_GETCHECK, 0, 0);
            EnableWindow(GetDlgItem(h, IDC_WATCHDOG_RATE), bEnabled);
            /* EnableWindow(GetDlgItem(h, IDC_SEND_ALBUMS), bEnabled); */

            break;
        }

        case IDC_PROXY_AUTH:
        {
            bool bAuthEnabled = MESS(IDC_PROXY_AUTH, BM_GETCHECK, 0, 0);

            EnableWindow(GetDlgItem(h, IDC_PROXY_USER), bAuthEnabled);
            EnableWindow(GetDlgItem(h, IDC_PROXY_PASSWORD), bAuthEnabled);

            break;
        }

        case IDC_LOGFILE_LIMIT:
        {
            bool bEnabled = MESS(IDC_LOGFILE_LIMIT, BM_GETCHECK, 0, 0);

            EnableWindow(GetDlgItem(h, IDC_LOGFILE_LIMITS), bEnabled);

            break;
        }

        case IDC_VIEW_LOG:
        {
            ShellExecute( h, "open", pathLog.c_str(), NULL, NULL, SW_SHOW );
            break;
        }

        case IDC_DELETE_LOG:
        {
            XMP_ClearLog();
            break;
        }
        }

        break;
    }

    case WM_INITDIALOG:
    {
        MESS(IDC_ENABLE, BM_SETCHECK, xmpcfg.on ? 1 : 0, 0);

        MESS(IDC_NOT_MZ_ABUSIVE, BM_SETCHECK, xmpcfg.MZ_Not_Abusive ? 1 : 0, 0);
        MESS(IDC_ENABLE_RADIO_SCROBBLING, BM_SETCHECK, xmpcfg.RadioScrobbling ? 1 : 0, 0);
        MESS(IDC_PROXY_ENABLE, BM_SETCHECK, xmpcfg.proxy_enabled ? 1 : 0, 0);
        MESS(IDC_PROXY_AUTH, BM_SETCHECK, xmpcfg.proxy_auth_enabled ? 1 : 0, 0);
        MESS(IDC_SEND_CHANGES, BM_SETCHECK, xmpcfg.SendChanges ? 1 : 0, 0);
        MESS(IDC_SEND_ALBUMS, BM_SETCHECK, xmpcfg.SendAlbums ? 1 : 0, 0);

        MESS(IDC_USERNAME, WM_SETTEXT, 0, xmpcfg.username);
        MESS(IDC_PASSWORD, WM_SETTEXT, 0, xmpcfg.password);

        MESS(IDC_PROXY_SERVER, WM_SETTEXT, 0, xmpcfg.proxy_server);
        MESS(IDC_PROXY_PORT, WM_SETTEXT, 0, xmpcfg.proxy_port);

        MESS(IDC_PROXY_USER, WM_SETTEXT, 0, xmpcfg.proxy_user);
        MESS(IDC_PROXY_PASSWORD, WM_SETTEXT, 0, xmpcfg.proxy_password);

        MESS(IDC_WATCHDOG_RATE, WM_SETTEXT, 0, xmpcfg.watchdog_rate);

        EnableWindow(GetDlgItem(h, IDC_PROXY_SERVER), xmpcfg.proxy_enabled);
        EnableWindow(GetDlgItem(h, IDC_PROXY_PORT), xmpcfg.proxy_enabled);

        EnableWindow(GetDlgItem(h, IDC_PROXY_AUTH), xmpcfg.proxy_enabled);
        EnableWindow(GetDlgItem(h, IDC_PROXY_USER), xmpcfg.proxy_enabled && xmpcfg.proxy_auth_enabled);
        EnableWindow(GetDlgItem(h, IDC_PROXY_PASSWORD), xmpcfg.proxy_enabled && xmpcfg.proxy_auth_enabled);

        EnableWindow(GetDlgItem(h, IDC_WATCHDOG_RATE), xmpcfg.RadioScrobbling);

        MESS(IDC_LOGFILE_LIMIT, BM_SETCHECK, xmpcfg.logfile_limit ? 1 : 0, 0);
        MESS(IDC_LOGFILE_TRUNCATE, BM_SETCHECK, xmpcfg.logfile_truncate ? 1 : 0, 0);
        MESS(IDC_LOGFILE_DEBUGMODE, BM_SETCHECK, xmpcfg.logfile_debugmode ? 1 : 0, 0);

        EnableWindow(GetDlgItem(h, IDC_LOGFILE_LIMITS), xmpcfg.logfile_limit);

        SendMessage(GetDlgItem(h, IDC_LOGFILE_LIMITS), CB_ADDSTRING, 0, (LPARAM) "32 KB");
        SendMessage(GetDlgItem(h, IDC_LOGFILE_LIMITS), CB_ADDSTRING, 0, (LPARAM) "64 KB");
        SendMessage(GetDlgItem(h, IDC_LOGFILE_LIMITS), CB_ADDSTRING, 0, (LPARAM) "128 KB");
        SendMessage(GetDlgItem(h, IDC_LOGFILE_LIMITS), CB_ADDSTRING, 0, (LPARAM) "256 KB");
        SendMessage(GetDlgItem(h, IDC_LOGFILE_LIMITS), CB_ADDSTRING, 0, (LPARAM) "512 KB");
        SendMessage(GetDlgItem(h, IDC_LOGFILE_LIMITS), CB_ADDSTRING, 0, (LPARAM) "1 MB");
        SendMessage(GetDlgItem(h, IDC_LOGFILE_LIMITS), CB_ADDSTRING, 0, (LPARAM) "2 MB");

        SendMessage(GetDlgItem(h, IDC_LOGFILE_LIMITS), CB_SETCURSEL, xmpcfg.logfile_limit_size, 0);

        return 1;
    }
    }

    return 0;
}

// show config options
void WINAPI DSP_Config(void */*inst*/, HWND win)
{
    DialogBox(hinst, (char*)IDD_CONFIG,win,&DSPDialogProc);
}
// get DSP config
DWORD WINAPI DSP_GetConfig(void */*inst*/, void *config)
{
    memcpy( config, &xmpcfg, sizeof(XMPScrobblerConfig) );
    return sizeof(xmpcfg);
}
// set DSP config
BOOL WINAPI DSP_SetConfig(void */*inst*/, void *config, DWORD size)
{
    if (size != sizeof(XMPScrobblerConfig) || config == nullptr)
    {
        XMP_Log( "[DEBUG] size of config not correct !, ignoring\n" );
        memset(&xmpcfg, 0, sizeof(XMPScrobblerConfig));
    }
    else
    {

        memcpy( &xmpcfg, config, sizeof(XMPScrobblerConfig) );

        if(xmpcfg.logfile_truncate)
            XMP_ClearLog();

    }
    XMP_Welcome();

    return TRUE;
}

void WINAPI DSP_Reset(void */*inst*/)
{
    if( !xmpcfg.on )
        return;

    XMP_Log( "[DEBUG] DSP_Reset\n" );

    XMP_KillSubmitTimer();
    XMP_SetDirty();
    if (XMP_IsCUE())
    {
        XMP_KillCUETimer();
        XMP_SetCUETimer();
    }

    return;
}

DWORD WINAPI DSP_Process(void */*inst*/, float */*buffer*/, DWORD count)
{
    if( !xmpcfg.on )
        return 0;

    if (xmpDoFetchinfo > 0)
    {
        --xmpDoFetchinfo;

        if (xmpDoFetchinfo == 0)
        {
            XMP_FetchInfo();

            /* Don't need this timer for radio streams */
            if (!XMP_IsRadio) {
                XMP_SetSubmitTimer();
            }
        }
    }

    //	XMP_Log( "[DEBUG] DSP_Process -- xmpCounter = %d, xmpChans = %d, xmpRate = %d\n", xmpCounter, xmpChans, xmpRate );

    if( xmpCounter != -1 )
    {
        int oldcnt = xmpCounter;

        xmpCounter -= count * xmpChans;

        if(oldcnt <= 0 )
        {
            XMP_Log( "[DEBUG] DSP_Process -- sec <= 0\n" );

            XMP_KillSubmitTimer();
            if(!XMP_IsRadio)
                XMP_SubmitProc();

            if(XMP_IsCUE())
                XMP_SetCUETimer();
        }
    }

    if(cueCounter != -1)
    {
        int oldcnt = cueCounter;

        cueCounter -= count * xmpChans;

        if(oldcnt <= 0 )
        {
            XMP_UpdateCurrentTrackIndex();
            XMP_CleanDirty();
            XMP_KillCUETimer();
            XMP_SetSubmitTimer();
        }
    }
    //XMP_Log( Stringify() << "[DEBUG] DSP_Process -- xmpCounter=" << xmpCounter << " cue=" << cueCounter << "\n");

    return 0;
}

void WINAPI DSP_SetFormat(void *inst, const XMPFORMAT *form)
{
    if( !xmpcfg.on )
        return;

    if(form != NULL) {
        XMP_Log(Stringify() << "[DEBUG] DSP_SetFormat( " << inst << ", " << form->rate << ", " << form->chan << " )\n");

        xmpRate = form->rate;
        xmpChans = form->chan;
    } else {
        XMP_Log(Stringify() << "[DEBUG] DSP_SetFormat( " << inst << ", NULL )\n");

        xmpRate = 0;
        xmpChans = 0;
    }

    if( xmpRate == 0 && xmpChans == 0 ) // track was stopped
    {
        XMP_Log( "[DEBUG] DSP_StopTrack\n" );

        /* We need re-run watchdog when user changes the radio stream */
        if(WatchdogCreated) {
            PleaseDie = true;
            XMP_IsRadio = false;
        }

        if(xmpCounter != -1)
            XMP_Log( "[INFO] Track stopped\n" );

        XMP_KillSubmitTimer();
        XMP_KillCUETimer();
    }
    else
    {
        XMP_SetDoFetchInfo();
    }
}

// new track has been opened (or closed if file=NULL)
void WINAPI DSP_NewTrack(void * /*inst*/, const char *file)
{
    if( !xmpcfg.on )
        return;

    XMP_ClearXMPFile();

    if( file != NULL )
    {
        XMP_Log( "[DEBUG] DSP_NewTrack (OPEN)\n" );

        XMP_ScrobInit( xmpcfg.username, xmpcfg.password, pathCache );

        XMP_KillSubmitTimer();
        XMP_KillCUETimer();

        XMP_SetDoFetchInfo();
    }
    else
    {
        XMP_Log( "[DEBUG] DSP_NewTrack (CLOSE)\n" );
    }
}

// the exported function that XMPlay calls to get the plugin interface
/*extern "C" __declspec( dllexport ) XMPDSP *WINAPI XMPDSP_GetInterface(DWORD face)
{
        return &dsp;
}*/

//#pragma comment(linker, "/EXPORT:XMPDSP_GetInterface2=_XMPDSP_GetInterface2@8")
extern "C" __declspec( dllexport ) XMPDSP *WINAPI XMPDSP_GetInterface2(DWORD face, InterfaceProc faceproc)
{
    if (face!=XMPDSP_FACE)
        return NULL;
    xmpfmisc=(XMPFUNC_MISC*)faceproc(XMPFUNC_MISC_FACE); // import "misc" functions
    xmpfstatus = (XMPFUNC_STATUS*)faceproc(XMPFUNC_STATUS_FACE);
    return &dsp;
}
extern "C" BOOL WINAPI DllMain(_In_ HINSTANCE hinstDLL, _In_ DWORD     fdwReason,_In_ LPVOID    /*lpvReserved*/)
{
    switch (fdwReason)
    {
    case DLL_PROCESS_ATTACH:
        hinst = hinstDLL;
        break;
    }
    return TRUE;
}


// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------

void XMP_Log(const std::string& s)
{
    if(!xmpcfg.logfile_debugmode && s.find("[DEBUG]") != std::string::npos)
        return;

    // if the log file is too big (depends on the maximum size set in options),
    // we need to clear its contents

    if(xmpcfg.logfile_limit && !XMP_ValidateLogSize())
        XMP_ClearLog();

    std::ofstream f( pathLog, std::ios::app );

    SYSTEMTIME time;

    GetLocalTime(&time);

    f << std::setfill('0');
    f << std::setw(2) << time.wYear << "-" << std::setw(2) << time.wMonth << "-" << std::setw(2)
      << time.wDay << " " << std::setw(2) << time.wHour << ":" << std::setw(2) << time.wMinute
      << ":" << std::setw(2) << time.wSecond << "\t";
    f << s;
}

void XMP_ClearLog()
{
    std::ofstream f(pathLog, std::ios::trunc | std::ios::out);
    f << "";
}

// checks if log size doesn't exceed maximum size set in options by the user

bool XMP_ValidateLogSize()
{
    std::ifstream f;

    f.open(pathLog, std::ios_base::binary | std::ios_base::in);

    if(!f.good() || f.eof() || !f.is_open())
        return true;

    f.seekg(0, std::ios_base::beg);
    std::ifstream::pos_type begin_pos = f.tellg();
    f.seekg(0, std::ios_base::end);

    int logSize = static_cast<int>(f.tellg() - begin_pos);

    int maxSize = 32 * 1024;
    int i = xmpcfg.logfile_limit_size;

    while(i-- > 0)
        maxSize *= 2;

    return logSize <= maxSize;
}

void XMP_SubmitProc()
{     
    XMP_Log( "[DEBUG] XMP_SubmitProc start\n" );

    if(XMP_IsDirty()) {
        return;
    }

    //In case plugin was disabled when track was loaded, this hasn't been done yet
    XMP_ScrobInit( xmpcfg.username, xmpcfg.password, pathCache );

    TRACKDATA curTrack;

    memset(&curTrack, 0, sizeof(curTrack));

    if(XMP_IsRadio)
    {
        const std::string& artist = RadioSt->get_artist_field().substr(0, sizeof(curTrack.artist) - 1);
        const std::string& title = RadioSt->get_title_field().substr(0, sizeof(curTrack.title) - 1);
        memmove(curTrack.artist, artist.c_str(), artist.length());
        memmove(curTrack.title, title.c_str(), title.length());
        if (xmpcfg.SendAlbums)
        {
            const std::string& album = RadioSt->get_album_field().substr(0, sizeof(curTrack.album) - 1);
            memmove(curTrack.album, album.c_str(), album.length());
        }
        /* Store track length recieved from Watchdog */
        curTrack.length = XMP_RadioTrackLength;
    }
    else
    {
        curTrack.length = xmpFile.alltracks[xmpFile.current_track].length;
        const std::string& artist = xmpFile.alltracks[xmpFile.current_track].artistName.substr(0, sizeof(curTrack.artist) - 1);
        const std::string& title = xmpFile.alltracks[xmpFile.current_track].songTitle.substr(0, sizeof(curTrack.title) - 1);
        const std::string& album = xmpFile.albumName.substr(0, sizeof(curTrack.album) - 1);
        const std::string& mb = xmpFile.mbId.substr(0, sizeof(curTrack.mb) - 1);
        memmove(curTrack.artist, artist.c_str(), artist.length());
        memmove(curTrack.title, title.c_str(), title.length());

        memmove(curTrack.album, album.c_str(), album.length());
        memmove(curTrack.mb, mb.c_str(), mb.length());
    }
    curTrack.playtime = time( NULL );
    curTrack.status = 0;

    scrob->addSong( curTrack );

    /* XXX: Or that could crash xmp */
    /* if(!xmpcfg.SendChanges)
          xmpFile.tracks[xmpFile.current_track].submitted = true; */
}

void XMP_ScrobInit( const char *u, const char *p, const std::string& cache )
{
    if( !scrob )
    {
        XMP_Log("[DEBUG] XMP_ScrobInit() started\n");

        ProxyConfig proxyConfig;
        if( xmpcfg.proxy_enabled )
        {
            proxyConfig.host = xmpcfg.proxy_server;
            proxyConfig.port = xmpcfg.proxy_port;
            if( xmpcfg.proxy_auth_enabled )
            {
                proxyConfig.username = xmpcfg.proxy_user;
                proxyConfig.password = xmpcfg.proxy_password;
            }
        }

        scrob.reset(new Scrobbler( u, p, "xmp", "1.1", cache, proxyConfig ));

        XMP_Log("[DEBUG] XMP_ScrobInit() finished\n");
    }
}

void XMP_ScrobTerm()
{
    scrob.reset();
}

// returns index of currently played track

int XMP_InTrack(int sec)
{
    int i;

    /*	for (i = xmpFile.alltracks.size() - 1; i >= 0; --i)
        {
                XMP_Log(Stringify() << "[DEBUG] XMP_InTrack:" << xmpFile.alltracks[i].start << "\n");
        }
        */
    for(i = xmpFile.alltracks.size() - 1; i >= 0; --i)
        if (xmpFile.alltracks[i].start <= sec)
            return i;

    return -1;
}

// gets information for the current file,
// fills xmpFile variable and parses info1 from XMPlay

unsigned int delay;

void XMP_FetchInfo()
{
    class xmpfmiscFree
    {
    public:
        void operator()(char* a) { xmpfmisc->Free(a); }
    };

    typedef std::unique_ptr<char, xmpfmiscFree> unique_ptr_xmpfmisc;

    XMP_Log( "[DEBUG] XMP_FetchInfo -- start\n" );

    unique_ptr_xmpfmisc info1(xmpfmisc->GetInfoText(XMPINFO_TEXT_MESSAGE));
    unique_ptr_xmpfmisc info2(xmpfmisc->GetTag(TAG_TITLE));
    //	unique_ptr_xmpfmisc info3(xmpfmisc->GetTag(TAG_ARTIST));
    unique_ptr_xmpfmisc info4(xmpfmisc->GetTag(TAG_ALBUM));
    //	unique_ptr_xmpfmisc info5(xmpfmisc->GetTag(TAG_TRACK));

    /*	if (info1)
                XMP_Log( Stringify() << "[AUX] info1: \n\n " << info1.get() << "\n\n");
        if (info2)
                XMP_Log(Stringify() << "[AUX] info2: \n\n " << info2.get() << "\n\n");
        if (info3)
                XMP_Log(Stringify() << "[AUX] info3: \n\n " << info3.get() << "\n\n");
        if (info4)
                XMP_Log(Stringify() << "[AUX] info4: \n\n " << info4.get() << "\n\n");
        if (info5)
                XMP_Log(Stringify() << "[AUX] info5: \n\n " << info5.get() << "\n\n");

*/
    if(!info1)
    {
        XMP_Log( "[DEBUG] XMP_FetchInfo -- GetInfoText failed!\n" );
        return;
    }

    xmpFile.length = SendMessage( xmpfmisc->GetWindow(), WM_USER, 1, 105 );

    const std::string& block = XMP_GetDataBlock( "Cues", info1.get() );

    // XMP_Log(Stringify() << "[DEBUG] block : " << block << "\n");

    if(!block.empty())
    {
        // this file contains CUE sheet, we need to parse it
        XMP_Log( "[DEBUG] XMP_FetchInfo() - CUE sheet parsing started\n" );

        unsigned int i;

        i = 0;

        xmpFile.alltracks.clear();

        static const std::regex matchtrack("^[0-9]+:[0-9]+.*$");
        static const std::regex matchstarttrack2("^([0-9]+):([0-9]+)$");
        static const std::regex matchstarttrack3("^([0-9]+):([0-9]+):([0-9]+)$");
        static const std::regex matchArtistTrack("^(.*) - (.*)$");

        for (size_t newcue_start, cue_start = 0 ; (newcue_start = block.find("\n", cue_start)) != std::string::npos ; cue_start = newcue_start + 1)
        {
            const auto line = block.substr(cue_start, newcue_start - (cue_start));
            const auto matched = std::regex_match(line, matchtrack);
            if (matched)
            {
                XMPTrackInfo info;

                const auto middleidx = line.find("\t");
                const auto start = line.substr(0, middleidx);
                const auto end = line.substr(middleidx + 1);

                std::smatch sm;
                const auto is3part = std::regex_match(start, sm, matchstarttrack3);
                if (is3part)
                {
                    info.start += 3600 * atoi(sm[1].str().c_str());
                    info.start += 60 * atoi(sm[2].str().c_str());
                    info.start += atoi(sm[3].str().c_str());
                }
                else
                {
                    const auto is2part = std::regex_match(start, sm, matchstarttrack2);
                    if (is2part == false)
                    {
                        XMP_Log(Stringify() << "[WARNING] cannot parse timestamp:" << start << "\n");
                        continue;
                    }
                    info.start += 60 * atoi(sm[1].str().c_str());
                    info.start += atoi(sm[2].str().c_str());
                }

                const auto albumartistok = std::regex_match(end, sm, matchArtistTrack);
                if (albumartistok == false)
                {
                    XMP_Log(Stringify() << "[WARNING] cannot parse track infos:" << end << "\n");
                    if (!end.empty())
                        info.songTitle = end;
                }
                else
                {
                    info.artistName = sm[1];
                    info.songTitle = sm[2];
                }


                if (i > 0)
                    xmpFile.alltracks[i - 1].length = info.start - xmpFile.alltracks[i - 1].start;

                info.tags_ok = true;

                xmpFile.alltracks.push_back(info);
                ++i;
            }
        }


        if (!info4 && info2) //true cue file
        {
            xmpFile.albumName = info2.get();
        }
        else if (info4)
        {
            xmpFile.albumName = info4.get();
        }


        // set length for the last track
        if (xmpFile.alltracks.size() > 0 )
            xmpFile.alltracks[xmpFile.alltracks.size() - 1].length = xmpFile.length - xmpFile.alltracks[xmpFile.alltracks.size() - 1].start;

        XMP_Log(Stringify() << "[INFO] CUE sheet found, current file contains " << xmpFile.alltracks.size() << " track(s)\n");
    }
    else
    {
        // we are dealing with a regular file (without CUE sheet),
        // so lets try to extract some data from tags

        XMPTrackInfo cue_track;

        cue_track.length = xmpFile.length;
        //		cue_track.end = xmpFile.length;

        RadioScrobbling = xmpcfg.RadioScrobbling;
        /* Not radio scrobbling by default */
        XMP_IsRadio = false;

        /* Check for network radio scrobbling. Treck length is unknown */
        if(!cue_track.tags_ok && SendMessage( xmpfmisc->GetWindow(), WM_USER, 1, 105 ) == 0)
            cue_track.tags_ok = XMP_ExtractTags_NetRadio(info1.get(), cue_track.artistName, cue_track.songTitle, xmpFile.albumName);
        /* Normal tracks go here */
        if(!XMP_IsRadio && SendMessage( xmpfmisc->GetWindow(), WM_USER, 1, 105 ) > 0) {
            cue_track.tags_ok = XMP_ExtractTags_ID3v2(info1.get(), cue_track.artistName, cue_track.songTitle, xmpFile.albumName);

            if(!cue_track.tags_ok)
                cue_track.tags_ok = XMP_ExtractTags_ID3v1(info1.get(), cue_track.artistName, cue_track.songTitle, xmpFile.albumName);

            if(!cue_track.tags_ok)
                cue_track.tags_ok = XMP_ExtractTags_WMA(info1.get(), cue_track.artistName, cue_track.songTitle, xmpFile.albumName);

            if(!cue_track.tags_ok)
                cue_track.tags_ok = XMP_ExtractTags_Other(info1.get(), cue_track.artistName, cue_track.songTitle, xmpFile.albumName);
        }
        else
        {
            /* Watchdog refresh rate*/
            xmpcfg.watchdog_rate[2] = '\0';
            delay = atoi(xmpcfg.watchdog_rate);
            if(!delay || delay < 9)
                delay = 9;
            else if(delay > 60)
                delay = 60;
            
            /* Create one instance of watchdog thread */
            if(!WatchdogCreated)
                CreateThread(NULL, 0, XMP_RadioWatchdog, NULL, 0, NULL);
        }

        /* Musicbrainz in streams? :D */
        if(cue_track.tags_ok && !XMP_IsRadio)
        {
            if (XMP_ExtractTags_MBID(info1.get(), xmpFile.mbId))
                XMP_Log(Stringify() << "[INFO] Track MusicBrainz ID: " << xmpFile.mbId << "\n");
            else if(!xmpcfg.MZ_Not_Abusive)
                XMP_Log( "[WARNING] No valid MusicBrainz ID found, consider using MusicBrainz taggers to tag this file properly!\n" );
        }
        else if(XMP_IsRadio) {
            if (!cue_track.artistName.empty() && !cue_track.songTitle.empty()) {
                XMP_Log(Stringify() << "[DEBUG] Recieved struct:\n" << info1.get());
                XMP_Log(Stringify() << "[INFO] Listening now to radio stream: " << cue_track.artistName << " - " << cue_track.songTitle << " (" << xmpFile.albumName << ")\n");
            }
        }
        if(cue_track.tags_ok)
        {
            xmpFile.alltracks.clear();

            xmpFile.alltracks.push_back(cue_track);

            xmpFile.current_track = 0;
        }
        else
        {
            XMP_Log("[WARNING] Current track is badly tagged - artist or title tag missing!\n");
        }
    }

    XMP_UpdateCurrentTrackIndex();

    XMP_Log( "[DEBUG] XMP_FetchInfo -- end\n" );
}

/* Radio stream watchdog */
DWORD WINAPI XMP_RadioWatchdog(void* /*vptr_args*/)
{    
    XMP_Log( "[DEBUG] XMP_RadioWatchdog -- start\n" );

    bool flag, StopLogged;
    /* Only one instance */
    WatchdogCreated = true;
    flag = StopLogged = false;
    time_t track_start_time;
    unsigned int consumpted = 0;
    std::string char_tmp, char_tmp2, char_tmp3;
    char *info1;

    /* Let's start count track length */
    time(&track_start_time);
    /* O to the M to the G */
PlaybackResumpted: 
    /* That's much better than killing thread from outside */
    while(XMP_IsRadio && !PleaseDie && !XMP_IsPaused() ) {
        /* WinAPI timer */
        Sleep(delay * 1000);
        
        if(! (info1 = xmpfmisc->GetInfoText(XMPINFO_TEXT_MESSAGE)) ) {
            XMP_Log( "[INFO] Radio stream stopped\n" );
            continue;
        }
        std::string tmp(info1);

        /* Can't get info. Possibly problems or stop playing? */
        if( !XMP_ExtractTags_NetRadio(info1, char_tmp, char_tmp2, char_tmp3) ) {
            if(!StopLogged)
                XMP_Log( "[DEBUG] Didn't find fields in the radio stream\n" );
            StopLogged = true;
            continue;
        }
        else
            StopLogged = false;
        /* Store title and artist for checking for updates */
        if(!flag)
            RadioSt->fill_fields(char_tmp, char_tmp2, char_tmp3);
        flag = true;
        std::string artist(char_tmp), title(char_tmp2);
        /* New track appeared */
        if(RadioSt->get_artist_field() != artist ||
                RadioSt->get_title_field() != title) {
            if(!PleaseDie || xmpcfg.SendChanges) {
                XMP_RadioNewTrack_flag = true;
                /* Let's store track length */
                XMP_RadioTrackLength = (unsigned int) difftime(time(NULL), track_start_time) - consumpted;
                consumpted = 0;
                /* Re-set start time */
                time(&track_start_time);
            }
        }
        /* New track detected, let's submit it */
        if(XMP_RadioNewTrack_flag) {
            XMP_RadioNewTrack_flag = flag = false;

            /* Actually, last.fm bans short tracks, but anyway, let's
             * don't break the rules */
            if(XMP_RadioTrackLength >= 30) {
                XMP_Log(Stringify() << "[INFO] Radio track for submission: " << RadioSt->get_artist_field() << " - " << RadioSt->get_title_field() << " (" << RadioSt->get_album_field() << ") "
                                                                                                                                                                                            "of approximately " << XMP_RadioTrackLength << "s length\n");
                /* Start submit proc */
                XMP_SubmitProc();
            } else
                XMP_Log(Stringify() << "[INFO] Radio track " << RadioSt->get_artist_field() << " - " << RadioSt->get_title_field() << " ("
                        << RadioSt->get_album_field() << ") of approximately " << XMP_RadioTrackLength	<< "s length willn't submit due of it's shortness\n");
        }
    }
    /* Just trick to prevent XMPlay from crashing.
      * Pauses are baneful */
    if(XMP_IsPaused() && XMP_IsRadio && !PleaseDie) {
        time_t start_time = time(NULL);
        /* Eating cycles, but nothing better else :( */
        while(XMP_IsPaused())
            ;
        /* Huh? Hindu code? Just rephrasing lmao */
        if(XMP_IsRadio && !PleaseDie) {
            /* We should substract idling time */
            consumpted = (unsigned int) difftime(time(NULL), start_time);
            goto PlaybackResumpted;
        }
    }

    XMP_Log( "[DEBUG] XMP_RadioWatchdog -- end\n" );

    /* Not sure that only once, we'll better re-run it when we'll need it again */
    WatchdogCreated = PleaseDie = false;
    return 1;
}

void XMP_SetSubmitTimer()
{
    //	XMP_Log( "[DEBUG] XMP_SetSubmitTimer()\n" );

    XMP_UpdateCurrentTrackIndex();

    if(xmpRate == 0 || xmpChans == 0 || xmpFile.current_track == -1)
        return;

    int len = xmpFile.alltracks[xmpFile.current_track].length;

    if( len < 30 )
    {
        XMP_Log( "[INFO] Track is too short to be submitted (must be at least 30 seconds long)\n" );

        if(XMP_IsCUE())
            XMP_SetCUETimer();

        return;
    }

    const auto playbackt = XMP_GetPlaybackTime();
    const int diff = playbackt - xmpFile.alltracks[xmpFile.current_track].start;
    //how come diff is of high value? it's probably because the playlist changed, so we won't resubmit it again
    const auto secLenTmp = (len / 2 < 240 ? len / 2 : 240);

    XMP_Log(Stringify() << "[DEBUG] play:" << playbackt << " diff: " << diff << " len: "<< secLenTmp<<"\n");

    if (diff >= secLenTmp)
    {
        XMP_Log("[INFO] Track is probably already submitted, skipping\n");

        if (XMP_IsCUE())
            XMP_SetCUETimer();

        return;
    }

    const auto secLen = secLenTmp - diff;


    XMP_KillSubmitTimer();

    if (XMP_IsDirty())
    {

        XMP_Log("[DEBUG] Track is dirty\n");

        if (XMP_IsCUE())
            XMP_SetCUETimer();

        return;
    }




    XMP_Log(Stringify() << "[DEBUG] lenTmp = " << secLenTmp << ", len = " << secLen << ", diff = " << diff << "\n");

    xmpCounter = secLen * xmpRate * xmpChans;

    xmpFile.alltracks[xmpFile.current_track].submitted = false;

    XMP_Log(Stringify() << "[INFO] Current track: " << xmpFile.alltracks[xmpFile.current_track].artistName << " - "
                                                                                                           << xmpFile.alltracks[xmpFile.current_track].songTitle << " (" << xmpFile.albumName << ")\n");

    XMP_Log(Stringify() << "[INFO] Submitting in " << secLen << " seconds...\n");
}

void XMP_KillSubmitTimer()
{
    //	XMP_Log( "[DEBUG] XMP_KillSubmitTimer\n" );

    xmpCounter = -1;
}

void XMP_SetCUETimer()
{
    XMP_Log(Stringify() << "[DEBUG] XMP_SetCUETimer start\n");
    XMP_KillCUETimer();

    XMP_UpdateCurrentTrackIndex();

    XMP_Log(Stringify() << "[DEBUG] XMP_SetCUETimer idx=" << xmpFile.current_track <<"\n");

    const int diff = XMP_GetPlaybackTime() - xmpFile.alltracks[xmpFile.current_track].start ;
    const int secLen = xmpFile.alltracks[xmpFile.current_track].length - diff + 5;

    cueCounter = secLen * xmpRate * xmpChans;

    XMP_Log(Stringify() << "[DEBUG] XMP_SetCUETimer(" << secLen << ")\n");
}

void XMP_KillCUETimer()
{
    cueCounter = -1;
}

bool XMP_IsCUE()
{
    const auto ret = xmpFile.alltracks.size() > 1;
    return ret;
}

int XMP_GetPlaybackTime()
{
    const auto t = xmpfstatus->GetTime();
    //XMP_Log(Stringify() << "[DEBUG] XMP_time(" << t << ")\n");
    return t;
    //return SendMessage( xmpfmisc->GetWindow(), WM_USER, 0, 105 ) / 1000;
}

bool XMP_IsPaused()
{
    /* Ian says use IPC_ISPLAYING to check is XMPlay playing something now
     * If pause detected then go for sleep */
    return ( SendMessage( xmpfmisc->GetWindow(), WM_USER, 0, IPC_ISPLAYING ) == 3 );
}

void XMP_UpdateCurrentTrackIndex()
{
    xmpFile.current_track = XMP_InTrack(XMP_GetPlaybackTime());
    //	XMP_Log("xmpFile.current_track = %d\n", xmpFile.current_track);
}

void XMP_CleanDirty()
{
    if (xmpFile.current_track != -1)
    {
        xmpFile.alltracks[xmpFile.current_track].dirty = false;
    }
}

void XMP_SetDirty()
{
    if(XMP_IsDirty())
        return;

    if (xmpFile.current_track != -1)
    {
        xmpFile.alltracks[xmpFile.current_track].dirty = true;
    }

    XMP_CancelFetchInfo();
    XMP_KillSubmitTimer();
    //XMP_KillCUETimer();

    if(xmpFile.current_track == -1 ||
            (xmpFile.current_track >= 0 && !xmpFile.alltracks[xmpFile.current_track].submitted &&
             xmpFile.alltracks[xmpFile.current_track].tags_ok))
        XMP_Log("[WARNING] Seeking during playback is not allowed - current file is not going to be submitted!\n");
}

bool XMP_IsDirty()
{
    if (xmpFile.current_track != -1)
    {
        return xmpFile.alltracks[xmpFile.current_track].dirty;
    }
    else
    {
        return false;
    }
}

bool XMP_WasResetDirty()
{
    return XMP_GetPlaybackTime() - xmpFile.alltracks[xmpFile.current_track].start > 5;
}

void XMP_ClearXMPFile()
{
    //reset
    xmpFile = XMPFileInfo();

}

void XMP_Welcome()
{
    DWORD dwVersion = GetVersion();

    DWORD dwMajor = (DWORD)(LOBYTE(LOWORD(dwVersion)));
    DWORD dwMinor = (DWORD)(HIBYTE(LOWORD(dwVersion)));
    DWORD dwBuild = 0; // Windows Me/98/95

    //	if(dwVersion < 0x80000000)
    dwBuild = (DWORD)(HIWORD(dwVersion));

    XMP_Log("----\n");
    XMP_Log(Stringify() << "[INFO] Hello, this is xmp-scrobbler " << XMPSCROBBLER_VERSION << "\n");
    XMP_Log(Stringify() << "[DEBUG] GetVersion() = " << GetVersion() << " (" << dwMajor << "." << dwMinor << ", build " << dwBuild << ")\n");
    XMP_Log(Stringify() << "[DEBUG] curl_version() = " << curl_version() << "\n");
}

void XMP_Setup()
{
    char path[255];

    GetModuleFileName( NULL, path, 255 );

    std::string spath( path );

    spath.erase( spath.find_last_of( "\\" ) + 1, std::string::npos );

    pathLog = spath + "xmp-scrobbler.txt";
    pathCache = spath + "xmp-scrobbler.cache";
}
