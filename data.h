#ifndef _XMP_SCROBBLER_DATA_H
#define _XMP_SCROBBLER_DATA_H

#include <string>

bool XMP_GetTagField(const std::string& data, const char *fn, std::string& buf);
std::string XMP_GetDataBlock(std::string name, const std::string& source);

bool XMP_ExtractTags_ID3v1(const char *data, std::string& artist, std::string& title, std::string& album);
bool XMP_ExtractTags_ID3v2(const char *data, std::string& artist, std::string& title, std::string& album);

bool XMP_ExtractTags_WMA(const char *data, std::string& artist, std::string& title, std::string& album);
bool XMP_ExtractTags_Other(const std::string& data, std::string& artist, std::string& title, std::string& album);
/* Internet radio scrobbling */
bool XMP_ExtractTags_NetRadio(const char *data, std::string& artist, std::string& title, std::string& album);
bool XMP_ExtractTags_MBID(const char *data, std::string& mb);

#endif
