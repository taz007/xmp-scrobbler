#include <string>
#include <iostream>
#include <fstream>

#include "cachemanager.h"
#include "xmp-scrobbler.h"

#include "stringify.h"

CacheManager::CacheManager(const std::string& cachefile) : m_cachefile(cachefile)
{
    Load();
}

CacheManager::~CacheManager()
{
    Save();
}

unsigned int CacheManager::size() const
{
    return m_tracks.size();
}

void CacheManager::AddTrack( const TRACKDATA& t )
{
	XMP_Log(Stringify() << "[DEBUG] AddTrack (cache_size = " << m_tracks.size() << ")\n");

	m_tracks.push_back(t);
}

void CacheManager::DeleteTracks( int n )
{
	m_tracks.erase(m_tracks.begin(), std::next(m_tracks.begin(),n));
}

bool CacheManager::Save()
{
	std::ofstream f(m_cachefile, std::ios::out | std::ios::trunc);
	if( !f.is_open() )
		return false;

	for( const auto& i : m_tracks )
		f.write( (char*) &i, sizeof(TRACKDATA) );

	return true;
}

bool CacheManager::Load()
{
	XMP_Log("[DEBUG] CacheManager::Load()\n");

	std::ifstream f( m_cachefile, std::ios::in | std::ios::binary );
	if( !f.is_open() ) 
		return false;

	f.seekg(0, std::ios::beg);
    const auto begin_pos = f.tellg();
  	f.seekg(0, std::ios::end);

	int len = static_cast<int>(f.tellg() - begin_pos);

	f.seekg(0, std::ios::beg);

	//check if old cache version

	if ((len % sizeof(TRACKDATA)) != 0)
	{
		XMP_Log("[WARNING] Old cache file version, skipping all cache entries\n");
		return false;
	}

	while( len > 0 )
	{
		TRACKDATA dt;
		f.read( (char*) &dt, sizeof(dt));
		m_tracks.push_front(dt);

		len -= sizeof(TRACKDATA);
	}

	XMP_Log(Stringify() << "[INFO] Number of entries in the cache: " << m_tracks.size() << "\n");

	return TRUE;
}

const CacheManager::ListTracks& CacheManager::GetSubmitPackage() const
{
    return m_tracks;
}
