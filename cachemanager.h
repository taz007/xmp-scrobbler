#ifndef _XMP_SCROBBLER_CACHE_MANAGER_H
#define _XMP_SCROBBLER_CACHE_MANAGER_H

#include <string>
#include <list>

#include "xmp-scrobbler.h"

class CacheManager
{
public:
    typedef std::list<TRACKDATA> ListTracks;

    explicit CacheManager(const std::string& cachefile);
    ~CacheManager();

    void AddTrack(const TRACKDATA& t);
    void DeleteTracks( int n );

    const ListTracks& GetSubmitPackage() const;
    unsigned int size() const;

private:
    bool Load();
    bool Save();


private:
    const std::string m_cachefile;
    ListTracks m_tracks;
};

#endif
