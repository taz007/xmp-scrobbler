SET OUTDIR=%1
SET CURLINCLUDE=C:\curl\include
SET CXXFLAGS=-std=c++14 -I"%CURLINCLUDE%" -Wall -Wextra -pedantic -O3 -pipe -flto
SET CURLLIB=C:\curl\build\Win32\VC12\DLL Release
SET LDFLAGS=-lcurl -L"%CURLLIB%"
g++ %CXXFLAGS% -c cachemanager.cpp -o "%OUTDIR%"\cachemanager.o
g++ %CXXFLAGS% -c data.cpp -o "%OUTDIR%"\data.o
g++ %CXXFLAGS% -c xmp-scrobbler.cpp -o "%OUTDIR%"\xmp-scrobbler.o
g++ %CXXFLAGS% -c libscrobbler/md5.c -o "%OUTDIR%"\md5.o
g++ %CXXFLAGS% -c libscrobbler/scrobbler.cpp -o "%OUTDIR%"\scrobbler.o
windres xmp-scrobbler.rc "%OUTDIR%"\xmp-scrobblerres.o
g++ -shared %LDFLAGS% -Wl,--kill-at -o "%OUTDIR%"\xmp-scrobbler.dll "%OUTDIR%"\cachemanager.o "%OUTDIR%"\data.o "%OUTDIR%"\xmp-scrobbler.o "%OUTDIR%"\md5.o "%OUTDIR%"\scrobbler.o "%OUTDIR%"\xmp-scrobblerres.o
strip -s "%OUTDIR%"\xmp-scrobbler.dll

