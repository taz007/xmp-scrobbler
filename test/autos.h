#ifndef AUTOS_H__
#define AUTOS_H__

#include <memory>
#include <windows.h>

class FreeLibraryFunctor
{
    public:

    typedef HMODULE pointer;

    void operator()(HMODULE a);
};
    
typedef std::unique_ptr<HMODULE, FreeLibraryFunctor > unique_ptr_Library;

#endif
