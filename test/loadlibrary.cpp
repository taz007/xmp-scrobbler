#include <windows.h>

#include <cstdlib>
#include <iostream>

#include "autos.h"

#include "xmpdsp.h"
#include "xmp-scrobbler.h"


#define FUNCTIONNAME "XMPDSP_GetInterface2"

typedef XMPDSP* (WINAPI *PXMPDSP_GetInterface2)(DWORD face, InterfaceProc faceproc);

void * WINAPI MyInterfaceProc(DWORD face)
{
    return nullptr;
}

int main(int argc, char* argv[])
{
    if(argc != 2)
    {
        std::cerr << "bad arguments" << std::endl;
        return EXIT_FAILURE;
    }

    //force scope
    {
        unique_ptr_Library ret(LoadLibrary(argv[1]));
        if (ret.get() == NULL)
        {
            const auto error = GetLastError();
            std::cerr << "LoadLibrary failed:" << error << std::endl;
            return EXIT_FAILURE;
        }

        const PXMPDSP_GetInterface2 pgi2 = (PXMPDSP_GetInterface2) GetProcAddress(ret.get(), FUNCTIONNAME);
        if (pgi2 == NULL)
        {
            const auto error = GetLastError();
            std::cerr << "GetProcAddress failed:" << error << std::endl;
            return EXIT_FAILURE;
        }


        XMPDSP* const xmpdsp = pgi2(XMPDSP_FACE, MyInterfaceProc);
        if (xmpdsp == nullptr)
        {
            std::cerr << "PXMPDSP_GetInterface2 failed" << std::endl;
            return EXIT_FAILURE;
        }

        const auto retNew = xmpdsp->New();
        if (retNew == nullptr)
        {
            std::cerr << "xmpdsp->New failed" << std::endl;
            return EXIT_FAILURE;
        }

        const char* const description = xmpdsp->GetDescription(retNew);
        if (description == nullptr || strlen(description) == 0)
        {
            std::cerr << "xmpdsp->GetDescription failed" << std::endl;
            return EXIT_FAILURE;
        }

        //null config for now
        BOOL retSetConfig = xmpdsp->SetConfig(retNew, nullptr, 0);
        if (retSetConfig == FALSE)
        {
            std::cerr << "xmpdsp->SetConfig failed" << std::endl;
            return EXIT_FAILURE;
        }

        XMPScrobblerConfig cfg;
        const auto sizeCfg = xmpdsp->GetConfig(retNew, &cfg);
        if (sizeCfg != sizeof(cfg))
        {
            std::cerr << "xmpdsp->GetConfig failed" << std::endl;
            return EXIT_FAILURE;
        }

        cfg.on = true;
        cfg.logfile_debugmode = true;

        retSetConfig = xmpdsp->SetConfig(retNew, &cfg, sizeof(cfg));
        if (retSetConfig == FALSE)
        {
            std::cerr << "xmpdsp->SetConfig failed" << std::endl;
            return EXIT_FAILURE;
        }

        xmpdsp->Free(retNew);

    }

    std::cout << "Test " << argv[0] << " finished." << std::endl;

}

