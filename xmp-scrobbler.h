#ifndef _XMP_SCROBBLER_H
#define _XMP_SCROBBLER_H

#include <windows.h>
#include <string>
#include <sstream>
#include "xmpdsp.h"

#define CACHE_PACKAGE_SIZE	10 // maximum number of tracks submitted at once

#define TAG_FIELD_SIZE		256

extern XMPFUNC_MISC *xmpfmisc;

//dont change/be dynamic yet as it needs to be easily serialized for now
struct TRACKDATA
{
	char artist[TAG_FIELD_SIZE];
	char title[TAG_FIELD_SIZE];
	char album[TAG_FIELD_SIZE];
	char mb[TAG_FIELD_SIZE];

	time_t playtime;
	DWORD length; // in seconds
	DWORD status; // -1 - too short, -2 - bad tags
};


void XMP_Log(const std::string& s);

struct XMPScrobblerConfig
{
    BOOL on;

    char username[255];
    char password[255];

    BOOL proxy_enabled;
    BOOL proxy_auth_enabled;

    char proxy_server[255];
    char proxy_port[10];

    char proxy_user[255];
    char proxy_password[255];

    BOOL logfile_limit;
    int logfile_limit_size;
    BOOL logfile_truncate;
    BOOL logfile_debugmode;

    BOOL MZ_Not_Abusive;
    BOOL RadioScrobbling;
    BOOL SendAlbums;
    char watchdog_rate[3];
    BOOL SendChanges;

};

#endif
