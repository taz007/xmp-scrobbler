/*
    This file is part of libscrobbler.

    libscrobbler is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    libscrobbler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with libscrobbler; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Copyright © 2003 Russell Garrett (russ-scrobbler@garrett.co.uk)
*/
#ifndef _SCROBBLER_H
#define _SCROBBLER_H

#include <mutex>

#include <windows.h>
#include <winbase.h>
#include <winuser.h>

#include <string>
#include <time.h>
#include <curl/curl.h>

#include <memory>

#include "md5.h"

#include "../xmp-scrobbler.h"
#include "../cachemanager.h"

#define LS_VER		"1.4"
#define MINLENGTH	30
#define MAXLENGTH	10800
#define HS_FAIL_WAIT 100000

/**
    An enumeration of the status messages so that clients can
    do things with them, and not have to parse the text
    to work out what's going on.
    If you don't care about these, then you can continue to
    just override the char* only version of statusUpdate
*/
enum ScrobbleStatus
{
    S_SUBMITTING = 0,

    S_NOT_SUBMITTING,

    S_CONNECT_ERROR,

    S_HANDSHAKE_SUCCESS,
    S_HANDSHAKE_UP_TO_DATE,
    S_HANDSHAKE_OLD_CLIENT,
    S_HANDSHAKE_INVALID_RESPONSE,
    S_HANDSHAKE_BAD_USERNAME,
    S_HANDSHAKE_ERROR,
    S_HANDHAKE_NOTREADY,

    S_SUBMIT_SUCCESS,
    S_SUBMIT_INVALID_RESPONSE,
    S_SUBMIT_INTERVAL,
    S_SUBMIT_BAD_PASSWORD,
    S_SUBMIT_FAILED,
    S_SUBMIT_BADAUTH,

    S_DEBUG

};

/**
    Audioscrobbler client class. Needs libCurl to run.
    $Id: scrobbler.h,v 1.14 2004/11/11 14:18:29 xurble Exp $

    @version	1.1
    @author		Russ Garrett (russ-scrobbler@garrett.co.uk)
*/


class HandleFreeFunctor
{
public:
    typedef HANDLE pointer;
    void operator()(HANDLE h);
};

typedef std::unique_ptr<HANDLE, HandleFreeFunctor> unique_ptr_handle;


class AutoMutex
{
public:
    explicit AutoMutex(HANDLE m);
    ~AutoMutex();
private:
    const HANDLE m_mutex;
};

class ProxyConfig
{
public:
    std::string host;
    std::string port;
    std::string username;
    std::string password;
};

template<class T> class AutoLock;

template<class T> class SharedDataProtector
{
    friend class AutoLock<T>;
public:

private:
    T m_t;
    std::mutex m_mutex;
};

template<class T> class AutoLock
{
public:
    explicit AutoLock(SharedDataProtector<T>& t) : m_t(t.m_t), m_lock(t.m_mutex) { }
    T* operator->() { return &m_t; }

private:
    T& m_t;
    std::lock_guard<std::mutex> m_lock;
};


class Scrobbler
{
public:

    /**
        Use this to initially set up the username and password. This sends a
        handshake request to the server to determine the submit URL and
        initial submit interval, and submissions will only be sent when
        it recieves a response to this.

        @param user		The user's Audioscrobbler username.
        @param pass		The user's Audioscrobbler password.
        @param id		The 3-letter client id of this client.
        @param ver		The version of this client.
        @see			setPassword()
        @see			setUsername()
    */
    Scrobbler(const char *user, const char *pass, const char* id, const char *ver, const std::string& cache, const ProxyConfig& proxyConfig);
    ~Scrobbler();

    //	void XMP_Log( const char *s, ... );

    /**
        Call this to add a song to the submission queue. The submission will get
        sent immediately unless the server has told it to cache, in which case it
        will get sent with the first song submission after the cache period has
        expired.

        @note Submission is not synchronous, so song submission status is only available through the statusUpdate callback.
        @note There's no way to tell the class to submit explicitly, it'll try to submit when you addSong.

        @param artist	The artist name.
        @param title	The title of the song.
        @param length	The length of the song in seconds.
        @param ltime	The time (unix time) in seconds when playback of the song started.
        @param trackid	The MusicBrainz TrackID of the song (in guid format).
        @see			statusUpdate()
        @retval 0		Failure. This could be because the song submitted was too short.
        @retval	1		This tripped song submission, Scrobbler is now connecting.
        @retval 2		The submission was cached.
    */
    //	int addSong(const char *artist, const char *title, const char *album, char mbid[37], int length, time_t ltime);
    void addSong( const TRACKDATA& track );

private: static std::string MD5Password(const char *pass);
private: static std::string curlEscape(const char *user);
private: void setProxy(const ProxyConfig& proxConf);

private:

    std::unique_ptr<CacheManager> cm;

    //result string + number of handled tracks (all at the time of the call)
    std::pair<std::string, unsigned int> prepareSubmitString();

    void statusUpdate(ScrobbleStatus status, const std::string& text);
    void statusUpdate(const std::string& text);

    void setInterval(int in);

    void genSessionKey(const std::string& challenge);

    void doSubmit();
    void doHandshake();

    // call back functions from curl
    void handleHeaders(const char* header);
    void handleHandshake(const char *handshake);
    void handleSubmit(const char *data);

    void workerThread();

private: std::string getHandShakeString() const;
private: std::string getUserAgentString() const;

    static DWORD WINAPI threadProc(void *param);
    static size_t hs_write_data(void *data, size_t size, size_t nmemb, void *userp);
    static size_t submit_write_data(void *data, size_t size, size_t nmemb, void *userp);
    static size_t curl_header_write(void *data, size_t size, size_t nmemb, void *userp);

    char curlerror[CURL_ERROR_SIZE];

    const std::string username;
    const std::string password; // MD5 hash
    std::string sessionkey;
    const std::string clientid;
    const std::string clientver;
    std::string proxyserver;
    std::string proxyinfo;

    class ThreadContextVars
    {
    public:
        std::string submiturl;
        int lastRetCode;
        std::string submitcontext;
        std::string handshakecontext;
    };

    class SharedData
    {
    public:
        bool closethread;
        time_t interval;
        bool readytosubmit;

        unsigned int currentSubmitSize;
        std::string submitString;
        bool submitinprogress;
    };
    typedef AutoLock<SharedData> AutoLockShared;

    SharedDataProtector<SharedData> sharedData;

    ThreadContextVars threadContextVars;

    unique_ptr_handle worker;
    unique_ptr_handle workerevent;

    std::mutex sharedDataMutex;

    time_t lastconnect;

};

#endif
