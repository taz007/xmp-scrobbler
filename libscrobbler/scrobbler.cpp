/*
    This file is part of libscrobbler.

    libscrobbler is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    libscrobbler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with libscrobbler; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

        Copyright © 2003 Russell Garrett (russ-scrobbler@garrett.co.uk)
        Copyright © 2015 T`aZ (taz.007@zoho.com) for Xmp-Scrobbler project
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <memory>
#include <utility>
#include <mutex>
#include "scrobbler.h"
#include "errors.h"
#include "../stringify.h"

class CurlFreeFunctor
{
public:
    void operator()(char* a) { curl_free(a); }
};

class CurlCleanUpFunctor
{
public:
    void operator()(CURL* a) { curl_easy_cleanup(a); }
};

typedef std::unique_ptr<char, CurlFreeFunctor > unique_ptr_curl;
typedef std::unique_ptr<CURL, CurlCleanUpFunctor > unique_ptr_curl_easy;

void HandleFreeFunctor::operator()(HANDLE h)
{
    if(h != INVALID_HANDLE_VALUE && h != NULL)
    {
        CloseHandle(h);
    }
}

AutoMutex::AutoMutex(HANDLE mutex) : m_mutex(mutex)
{
    WaitForSingleObject(m_mutex, INFINITE);
}
AutoMutex::~AutoMutex()
{
    ReleaseMutex(m_mutex);
}

// -----------------------------------------------------------
Scrobbler::Scrobbler(const char *user, const char *pass, const char *id, const char *ver, const std::string& cache, const ProxyConfig& proxyConfig)
    : username(curlEscape(user)),
      password(MD5Password(pass)),
      clientid(id),
      clientver(ver)
{
    {
        AutoLockShared shared(sharedData);
        shared->interval = 0;
        shared->closethread = false;
        shared->submitinprogress = false;
        shared->currentSubmitSize = 0;
        shared->readytosubmit = false;
    }

    lastconnect = 0;

    DWORD threadid; // Needed for Win9x
    workerevent.reset(CreateEvent(NULL, false, false, NULL));
    if (!workerevent.get())
        throw EOutOfMemory();
    worker.reset(CreateThread(NULL, 0, threadProc, (LPVOID)this, 0, &threadid));
    if (!worker.get())
        throw EOutOfMemory();

    setProxy(proxyConfig);

    cm.reset(new CacheManager( cache ));

    XMP_Log("[DEBUG] Scrobbler::init() - cache loaded, doing handshake...\n");

    doHandshake();

}

// -----------------------------------------------------------
Scrobbler::~Scrobbler()
{
    {
        AutoLockShared a(sharedData);
        a->closethread = true;
    }

    SetEvent(workerevent.get());
    WaitForSingleObject(worker.get(), INFINITE);
}

// -----------------------------------------------------------
std::string Scrobbler::MD5Password(const char *pass)
{
    std::string password;

    const auto& len = strlen(pass);
    if (len > 0)
    {
        md5_state_t md5state;
        unsigned char md5pword[16];
        md5_init(&md5state);
        md5_append(&md5state, (unsigned const char *)pass, (int)len);
        md5_finish(&md5state, md5pword);
        char tmp[33] = { 0 };

        for (int j = 0;j < 16;j++)
        {
            char a[3];
            sprintf(a, "%02hhx", md5pword[j]);
            tmp[2*j] = a[0];
            tmp[2*j+1] = a[1];
        }
        password = tmp;
    }

    return password;
}

// -----------------------------------------------------------
std::string Scrobbler::curlEscape(const char *user)
{
    std::string username;

    const auto& len = strlen(user);
    if (len > 0)
    {
        unique_ptr_curl u(curl_escape(user,len));
        username = u.get();
    }

    return username;
}

std::string Scrobbler::getHandShakeString() const
{
    return Stringify() << "http://post.audioscrobbler.com/?hs=true&p=1.1&c=" << clientid << "&v=" << clientver << "&u=" << username;
}

std::string Scrobbler::getUserAgentString() const
{
    return Stringify() << "Mozilla/5.0 (compatible; libscrobbler " << LS_VER << "; " << clientid << " " << clientver << ")";
}


// -----------------------------------------------------------
void Scrobbler::setProxy(const ProxyConfig& proxConf)
{
    if( proxConf.username.empty() == true && proxConf.password.empty() == true )
    {
        proxyinfo.clear();
    }
    else
    {
        proxyinfo = proxConf.username + ":" + proxConf.password;
    }

    if( proxConf.host.empty() ) // no proxy server
    {
        proxyserver.clear();
        proxyinfo.clear();
    }
    else
    {
        proxyserver = proxConf.host + ":" + proxConf.port;

        std::string temp = "Using proxy server: " + proxyserver;

        if( proxyinfo.empty() == false )
            temp += " (with authentication - user and password are set in options)";

        statusUpdate(S_DEBUG,temp);
    }
}

void Scrobbler::statusUpdate(ScrobbleStatus /*status*/, const std::string& text)
{
    //if(status != S_DEBUG) // don't pass out pointless debug messages to foobar
    statusUpdate(text);
}

void Scrobbler::statusUpdate(const std::string& text)
{
    XMP_Log( Stringify() << "[DEBUG] " << text << "\n" );
}



std::pair<std::string, unsigned int> Scrobbler::prepareSubmitString()
{
    const auto& current_sp = cm->GetSubmitPackage();
    std::ostringstream submitStr;

    XMP_Log(Stringify() << "[INFO] Preparing to submit " << current_sp.size() << " track(s) from the cache\n");
    int count = 0;
    for (const auto& track : current_sp)
    {
        char ti[20];
        struct tm today;
        if (gmtime_s(&today, &track.playtime) != 0)
        {
            XMP_Log(Stringify() << "[WARNING] Unable to parse playtime:" << track.playtime << ", skipping entry!\n");
            continue;
        }

        if (strftime(ti, sizeof(ti), "%Y-%m-%d %H:%M:%S", &today) == 0)
        {
            XMP_Log(Stringify() << "[WARNING] Unable to parse playtime:" << track.playtime << ", skipping entry!\n");
            continue;
        }



        unique_ptr_curl a( curl_escape(track.artist, (int)strlen(track.artist)));
        unique_ptr_curl b( curl_escape(track.album, (int)strlen(track.album)));
        unique_ptr_curl t( curl_escape(track.title, (int)strlen(track.title)));
        unique_ptr_curl i( curl_escape(ti, (int)strlen(ti)));
        unique_ptr_curl m( curl_escape(track.mb, (int)strlen(track.mb)));

        if (!a || !b || !t || !i || !m)
        {
            XMP_Log(Stringify() << "[WARNING] Unable to curl_escape some data, skipping entry!\n");
            continue;
        }

        submitStr << "&a[" << count << "]=" << a.get() << "&b[" << count << "]=" << b.get()
                  << "&t[" << count << "]=" << t.get() << "&i[" << count << "]=" << i.get() << "&l[" << count << "]=" << track.length
                  << "&m[" << count << "]=" << m.get();

        ++count;
    }

    return std::make_pair(submitStr.str(), current_sp.size());
}
// -----------------------------------------------------------
//int Scrobbler::addSong(const char *artist, const char *title, const char *album, char mbid[37], int length, time_t ltime)
void Scrobbler::addSong( const TRACKDATA& track )
{
    cm->AddTrack( track );

    XMP_Log("[INFO] Track added to the cache for submission\n");

    time_t interv;
    bool inprogress;
    bool ready;

    {
        AutoLockShared a(sharedData);
        interv = a->interval;
        inprogress = a->submitinprogress;
        ready = a->readytosubmit;
    }

    if(inprogress)
    {
        statusUpdate(S_NOT_SUBMITTING, "Previous submission still in progress");
        return;
    }

    time_t now;
    time (&now);

    if ((interv + lastconnect) < now)
    {
        if (!ready)
        {
            doHandshake();
        }
        else
        {
            doSubmit();
        }
    }
    else
    {
        const std::string& msg = Stringify() << "Not submitting, caching for " << (interv + lastconnect - now) << " more second(s). Cache has " << cm->size() << " entries.";
        statusUpdate(S_NOT_SUBMITTING, msg);
    }
}

// -----------------------------------------------------------
void Scrobbler::doHandshake()
{
    {
       AutoLockShared a(sharedData);
        a->readytosubmit = false;
    }

    time (&lastconnect);

    SetEvent(workerevent.get());
}

// -----------------------------------------------------------
void Scrobbler::doSubmit()
{
    bool inprogress;
    bool readySubmit;
    {
        AutoLockShared a(sharedData);
        inprogress = a->submitinprogress;
        readySubmit = a->readytosubmit;
    }

    if(inprogress == true)
    {
        statusUpdate(S_NOT_SUBMITTING,"Previous submission still in progress");
        return;
    }

    if (username.empty() || password.empty() || !readySubmit)
    {
        statusUpdate(S_NOT_SUBMITTING,"user or password empty or not ready ?!");
        return;
    }

    statusUpdate(S_SUBMITTING,"Submitting...");

    const auto& submitInfo = prepareSubmitString();
    const auto& poststring = submitInfo.first;
    const auto& size = submitInfo.second;

    {
        AutoLockShared a(sharedData);
        a->submitinprogress = true;
        a->currentSubmitSize = size;
        a->submitString = "u=" + username + "&s=" + sessionkey + poststring;
    }



    time (&lastconnect);

    SetEvent(workerevent.get());
}

std::vector<std::string> tokenize(const std::string& input, const std::string& delims)
{
    std::vector<std::string> ret;
    std::string::size_type start = 0;
    while (true)
    {
        const auto& sepptr = input.find_first_of(delims,start);
        ret.push_back(input.substr(start, sepptr - (start)));
        if (sepptr == std::string::npos)
            break;
        start = sepptr +1;

    }
    return ret;
}
// -----------------------------------------------------------
void Scrobbler::handleHandshake(const char *handshakepartialdata)
{
    // Doesn't take into account multiple-packet returns (not that I've seen one yet)...
    //now it does :(, fugly hack to handle it

    // Ian says: strtok() is not re-entrant, but since it's only being called
    //  in only one function at a time, it's ok so far.

    if(threadContextVars.lastRetCode != 200)
    {
        // no point in doing this unless we get an OK
        //statusUpdate(S_DEBUG,"NOT 200");
        return;
    }

    if (handshakepartialdata!= nullptr)
    {
        threadContextVars.handshakecontext += handshakepartialdata;
    }
    else
    {
        statusUpdate(S_DEBUG, "data received null?!");
    }

    static const std::string separators = " \n\r";

    const auto& tokens = tokenize(threadContextVars.handshakecontext, separators);

    if (tokens.size() < 1 || tokens[0] == "")
    {
        statusUpdate(S_HANDSHAKE_INVALID_RESPONSE,"Handshake failed: Response invalid");
        return;
    }
    do {
        const auto& response = tokens[0];

        if (_stricmp("UPTODATE", response.c_str()) == 0)
        {
            statusUpdate(S_HANDSHAKE_UP_TO_DATE,"Handshaking: Client up to date.");
        }
        else if (_stricmp("UPDATE", response.c_str()) == 0)
        {
            if (tokens.size() < 2 || tokens[1] == "")
                break;

            const auto& updateurl = tokens[1];
            std::string msg = "Handshaking: Please update your client at: ";
            msg += updateurl;
            statusUpdate(S_HANDSHAKE_OLD_CLIENT,msg);
        }
        else if (_stricmp("BADUSER", response.c_str()) == 0)
        {
            statusUpdate(S_HANDSHAKE_BAD_USERNAME,"Handshake failed: Bad username");
            return;
        }
        else
        {
            break;
        }
        if (tokens.size() < 5)
        {
            XMP_Log("[DEBUG] Not enough info yet, retrying later\n");
            return;
        }
        const auto& challenge = tokens[1];
        threadContextVars.submiturl = tokens[2];

        statusUpdate(S_DEBUG, threadContextVars.submiturl);

        if ( !(_stricmp("INTERVAL", tokens[3].c_str()) == 0))
            break;
        const auto& interv = tokens[4];
        setInterval(atoi(interv.c_str()));
        genSessionKey(challenge);
        {
            AutoLockShared a(sharedData);
            a->readytosubmit = true;
        }
        threadContextVars.handshakecontext.clear();
        statusUpdate(S_HANDSHAKE_SUCCESS,"Handshake successful.");
        XMP_Log("[INFO] Handshake with Last.fm server successful!\n");
        return;
    } while (0);

    const std::string& buf = "Handshake failed: " + threadContextVars.handshakecontext;
    threadContextVars.handshakecontext.clear();
    statusUpdate(S_HANDSHAKE_ERROR,buf);
    XMP_Log("[WARNING] Handshake with Last.fm server failed!\n");
}

// -----------------------------------------------------------
void Scrobbler::handleSubmit(const char *data)
{
    //	submit returned
    {
        AutoLockShared a(sharedData);
        a->submitinprogress = false; //- this should already have been cancelled by the header callback
    }

    if(threadContextVars.lastRetCode != 200)
    {
        // there's no real point in checking this is there?
        return;
    }

    if (data != nullptr)
    {
        threadContextVars.submitcontext += data;
    }
    else
    {
        XMP_Log("[DEBUG] no data ?!\n");
    }

    // Doesn't take into account multiple-packet returns (not that I've seen one yet)...
    static const std::string seps = " \n\r";

    const auto& tokens = tokenize(threadContextVars.submitcontext, seps);

    if (tokens.size() < 1)
    {
        statusUpdate(S_SUBMIT_INVALID_RESPONSE,"Submission failed: Response invalid");
        return;
    }
    const auto& response = tokens[0];

    if (_stricmp("OK", response.c_str()) == 0)
    {
        if (tokens.size() < 3)
        {
            XMP_Log("[DEBUG] Not enough info yet\n");
            return;
        }

        statusUpdate(S_SUBMIT_SUCCESS,"Submission succeeded.");
        XMP_Log("[INFO] Submission succeeded!\n");

        unsigned int size;
        {
            AutoLockShared a(sharedData);
            size = a->currentSubmitSize;
        }

        cm->DeleteTracks( size );
        if (_stricmp("INTERVAL", tokens[1].c_str()) == 0)
        {
            setInterval(atoi(tokens[2].c_str()));
        }
    }
    else if (_stricmp("BADPASS", response.c_str()) == 0)
    {
        statusUpdate(S_SUBMIT_BAD_PASSWORD,"Submission failed: bad password, retrying");
    }
    else if (_stricmp("FAILED", response.c_str()) == 0)
    {
        if (tokens.size() < 4)
        {
            XMP_Log("[DEBUG] Not enough info yet\n");
            return;
        }
        const std::string& buf = "Submission failed: " + tokens[1];
        statusUpdate(S_SUBMIT_FAILED, buf);

        if (_stricmp("INTERVAL", tokens[2].c_str()) == 0)
        {
            setInterval(atoi(tokens[3].c_str()));
        }
    }
    else if (_stricmp("BADAUTH",response.c_str()) == 0)
    {
        statusUpdate(S_SUBMIT_BADAUTH,"Submission failed: bad authorization.");
        {
            AutoLockShared a(sharedData);
            a->readytosubmit = false;
        }
    }
    else
    {
        const std::string& buf = Stringify() << "Submission failed: " << data;
        statusUpdate(S_SUBMIT_FAILED,buf);
    }
    threadContextVars.submitcontext.clear();
}

// -----------------------------------------------------------
void Scrobbler::setInterval(int in)
{
    {
        AutoLockShared a(sharedData);
        a->interval = in;
    }

    const std::string& ret = Stringify() << "Submit interval set to " << in << " second(s).";
    statusUpdate(S_SUBMIT_INTERVAL, ret);
}

void Scrobbler::genSessionKey(const std::string& challenge)
{
    const std::string& clear = password + challenge;
    md5_state_t md5state;
    unsigned char md5pword[16];
    md5_init(&md5state);
    md5_append(&md5state, (unsigned const char *)clear.c_str(), (int)clear.length());
    md5_finish(&md5state, md5pword);
    char key[33] = { 0 };

    for (int j = 0;j < 16;j++) {
        char a[3];
        sprintf(a, "%02hhx", md5pword[j]);
        key[2*j] = a[0];
        key[2*j+1] = a[1];
    }
    sessionkey = key;
}

void Scrobbler::handleHeaders(const char* header)
{
    {
        AutoLockShared a(sharedData);
        a->submitinprogress = false;
    }


    std::string code;
    std::string headerStr = header;
    //   statusUpdate(S_DEBUG,header);

    if(headerStr.substr(0,4) == "HTTP") // this is the response code
    {
        //statusUpdate(S_DEBUG,header);
        size_t pos = headerStr.find(" ");

        if(pos != headerStr.npos)
        {
            code = headerStr.substr(pos+1);
            pos = code.find(" ");
            if(pos != code.npos)
            {
                code = code.substr(0,pos);
            }
            threadContextVars.lastRetCode = atoi(code.c_str());
            if(threadContextVars.lastRetCode != 200)
            {
                statusUpdate(S_CONNECT_ERROR,"The server reported a processing error.");
                statusUpdate(S_DEBUG,header);
            }
        }
    }

}

// -----------------------------------------------------------
void Scrobbler::workerThread()
{
    const auto& handshakeString = getHandShakeString();
    const auto& userAgentString = getUserAgentString();

    while (1)
    {
        WaitForSingleObject(workerevent.get(), INFINITE);

        //read data here and cache them
        bool copyClose;
        bool copyReadySubmit;
        std::string copySubmitStr;

        {
            AutoLockShared a(sharedData);
            copyClose = a->closethread;
            copyReadySubmit = a->readytosubmit;
            copySubmitStr = a->submitString;
        }
        if (copyClose == true)
            break;

        //	statusUpdate(S_DEBUG,"...");

        unique_ptr_curl_easy curl(curl_easy_init());

        curl_easy_setopt(curl.get(), CURLOPT_FORBID_REUSE, 1); // Forbid keepalive connections.
        curl_easy_setopt(curl.get(), CURLOPT_USERAGENT, userAgentString.c_str());
        curl_easy_setopt(curl.get(), CURLOPT_PROXY, proxyserver.c_str());
        curl_easy_setopt(curl.get(), CURLOPT_PROXYUSERPWD, proxyinfo.c_str());

        curl_easy_setopt(curl.get(), CURLOPT_HEADERFUNCTION, curl_header_write);
        curl_easy_setopt(curl.get(), CURLOPT_WRITEHEADER, this);


        if (proxyinfo.empty() == false)
        {
            curl_easy_setopt(curl.get(), CURLOPT_PROXYAUTH, CURLAUTH_ANY);
        }
        else
        {
            curl_easy_setopt(curl.get(), CURLOPT_PROXYAUTH, 0);
        }

        curl_easy_setopt(curl.get(), CURLOPT_TIMEOUT , 30);

        curl_slist *slist=NULL;


        if (copyReadySubmit == false)
        { //do handshake first
            curl_easy_setopt(curl.get(), CURLOPT_HTTPGET, 1);
            curl_easy_setopt(curl.get(), CURLOPT_URL, handshakeString.c_str());
            curl_easy_setopt(curl.get(), CURLOPT_WRITEFUNCTION, hs_write_data);
            curl_easy_setopt(curl.get(), CURLOPT_WRITEDATA, this);
            curl_easy_setopt(curl.get(), CURLOPT_VERBOSE, 1);
            curl_easy_setopt(curl.get(), CURLOPT_ERRORBUFFER, curlerror);

            threadContextVars.handshakecontext.clear();
        }
        else
        { //normal case
            curl_easy_setopt(curl.get(), CURLOPT_POSTFIELDS, copySubmitStr.c_str());
            curl_easy_setopt(curl.get(), CURLOPT_URL, threadContextVars.submiturl.c_str());
            curl_easy_setopt(curl.get(), CURLOPT_WRITEFUNCTION, submit_write_data);
            curl_easy_setopt(curl.get(), CURLOPT_WRITEDATA, this);
            curl_easy_setopt(curl.get(), CURLOPT_VERBOSE, 1);
            curl_easy_setopt(curl.get(), CURLOPT_ERRORBUFFER, curlerror);



            slist = curl_slist_append(slist, "Expect:");
            curl_easy_setopt(curl.get(), CURLOPT_HTTPHEADER, slist);

            //clear potential invalid partial response
            threadContextVars.submitcontext.clear();
        }

        const auto& success = curl_easy_perform(curl.get());

        if(slist)
            curl_slist_free_all(slist);

        if(success)
        {
            // non zero success actually means fail
            if(copyReadySubmit == true)
            {
                // failed to post, means post is over
               AutoLockShared a(sharedData);
                a->submitinprogress = false;
            }

            statusUpdate(S_CONNECT_ERROR,"Could not connect to server.");
            statusUpdate(S_CONNECT_ERROR,curlerror);
        }


    }
}

size_t Scrobbler::curl_header_write(void *data, size_t size, size_t nmemb, void *userp)
{
    try
    {
        //	the buffer returned is not null terminated (it could be binary data)
        char* in = static_cast<char *>(data);

        //	since we're all about the strings, we create a new buffer big enough
        //	to hold the old one + a null terminator
        std::unique_ptr<char[]> buffer (new char[nmemb*size+1]);

        //	and copy in the old buffer
        memcpy(buffer.get(),in,nmemb*size);
        buffer[nmemb*size] = 0;
        //ATLTRACE(buffer);

        //	then we pass it to our handler class
        static_cast<Scrobbler *>(userp)->handleHeaders(buffer.get());

    }
    catch(...){ static_cast<Scrobbler *>(userp)->statusUpdate(S_DEBUG,"EXCEPTION IN WRITE HEADER"); }
    return nmemb;
}

size_t Scrobbler::submit_write_data(void *data, size_t size, size_t nmemb, void *userp)
{
    try
    {
        //	the buffer returned is not null terminated (it could be binary data)
        char* in = static_cast<char *>(data);

        //	since we're all about the strings, we create a new buffer big enough
        //	to hold the old one + a null terminator
        std::unique_ptr<char[]> buffer (new char[nmemb*size+1]);

        //	and copy in the old buffer
        memcpy(buffer.get(),in,nmemb*size);
        buffer[nmemb*size] = 0;
        //ATLTRACE(buffer);

        //	then we pass it to our handler class
        static_cast<Scrobbler *>(userp)->handleSubmit(buffer.get());

    }
    catch(...){ static_cast<Scrobbler *>(userp)->statusUpdate(S_DEBUG,"EXCEPTION IN SUBMIT DATA"); }
    return nmemb;
}
size_t Scrobbler::hs_write_data(void *data, size_t size, size_t nmemb, void *userp)
{
    try
    {
        //	the buffer returned is not null terminated (it could be binary data)
        char* in = static_cast<char *>(data);

        //	since we're all about the strings, we create a new buffer big enough
        //	to hold the old one + a null terminator
        std::unique_ptr<char[]> buffer (new char[nmemb*size+1]);

        //	copy in the old buffer
        memcpy(buffer.get(),in,nmemb*size);
        buffer[nmemb*size] = 0;

        //	then we pass it to our handler class
        static_cast<Scrobbler *>(userp)->handleHandshake(buffer.get());

    }
    catch(...)
    {
        static_cast<Scrobbler *>(userp)->statusUpdate(S_DEBUG,"EXCEPTION IN HANDSHAKE DATA");
    }
    return nmemb*size;
}

DWORD WINAPI Scrobbler::threadProc(void *param)
{
    static_cast<Scrobbler*>(param)->workerThread();
    return 1;
}
