#ifndef STRINGIFY_H
#define STRINGIFY_H

#include <sstream>

class Stringify
{
public:
    Stringify() = default;
    Stringify(const Stringify& s) = delete;
    Stringify(Stringify&& s) = delete;
    Stringify& operator=(const Stringify& s) = delete;
    Stringify& operator=(Stringify&& s) = delete;

    operator std::string() const;

    template<class T>
    Stringify& operator<<(const T& s)
    {
        m_oss << s;
        return *this;
    }
private:
    std::ostringstream m_oss;
};

#endif // STRINGIFY_H
